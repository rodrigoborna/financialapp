import React from "react";
import Screen from "./Screen";

export const ProfileScreen = ({ navigation }) => <Screen navigation={navigation} name="Meus Dados" />;
export const MessageScreen = ({ navigation }) => <Screen navigation={navigation} name="Alertas" />;
export const ActivityScreen = ({ navigation }) => <Screen navigation={navigation} name="Lançamentos" />;
export const ListScreen = ({ navigation }) => <Screen navigation={navigation} name="Categorias" />;
export const ReportScreen = ({ navigation }) => <Screen navigation={navigation} name="Relatórios" />;
export const StatisticScreen = ({ navigation }) => <Screen navigation={navigation} name="Estatísticas" />;
export const SignOutScreen = ({ navigation }) => <Screen navigation={navigation} name="Sair" />;
