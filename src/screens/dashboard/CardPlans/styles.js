import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    width: "100%",
    padding: 10,
    alignItems:"center"
  },
  cards: {
    marginTop: 20,
    width: "100%",
    backgroundColor: "#ADD8E6",
    borderRadius: 12,
    padding: 20,
    alignItems: "center",
  },
  button: {
    width: "80%",
    paddingVertical: 20,
    backgroundColor: "#1C8BEB",
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
  },
});

export default styles;
