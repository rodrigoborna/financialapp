import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  cardMain: {
    width: "100%",
    padding: 10,
    alignItems:"center"
    
     },
  dataIcons: {
    marginTop: 20,
    flexDirection: "row",
    backgroundColor: "#ADD8E6",
    borderTopRightRadius: 12,
    borderTopLeftRadius: 12,
    justifyContent: "space-between",
    width: "100%",
    minHeight: 40,
    alignItems: "center",
    paddingHorizontal: 10,
  },
  dataBalance: {
    width: "100%",
    backgroundColor: "#ADD8E6",
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
    
  },
  dataValues: {
    width: "100%",
    backgroundColor: "#ADD8E6",
    flex: 1,
    flexDirection: "row",
    borderBottomRightRadius: 12,
    borderBottomLeftRadius: 12,
  },
});

export default styles;
