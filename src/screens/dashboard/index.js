import { StatusBar } from "expo-status-bar";
import React from "react";
import { SafeAreaView, ScrollView, TouchableOpacity, View, Text } from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";
import styles from "./styles";

import CardMain from "./CardMain";
import CardCards from "./CardCards";
import CardGraphics from "./CardGraphics";
import CardPlans from "./CardPlans";


export default class Screen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ flex: 1 }}>
          <TouchableOpacity
              style={{ alignItems: "flex-end",margin:16, marginTop: 30}}
              onPress={this.props.navigation.openDrawer}>
              <FontAwesome5 name="bars" size={24} color="#161924" />
            </TouchableOpacity>
          <ScrollView contentContainerStyle={{ paddingBottom: 100 }}>
            <CardCards />
            <CardMain />
            
            <CardGraphics />
            <CardPlans />
          </ScrollView>
        </SafeAreaView>
      </View>

    );
  }
}

